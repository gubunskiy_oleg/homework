package sbp.REST_Tests;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import org.junit.Assert;
import org.junit.Test;
import sbp.JDBC.Client_JDBC;
import sbp.REST.HelpHandler;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Executors;

import static org.junit.Assert.*;

public class HelpHandlerTest {

    /**
     * Emulating server and client. Starting server, sending query from client, checking response.
     */
    @Test
    public void handle() {

        final String HOSTNAME = "localhost";
        final int PORT = 1234;
        final String db = "jdbc:sqlite:src/main/java/sbp/jdbc/chinook.db";

        HttpServer server = null;
        try {
            server = HttpServer.create(new InetSocketAddress(HOSTNAME, PORT), 0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        server.createContext("/help", new HelpHandler());
        server.setExecutor(Executors.newSingleThreadExecutor());

        server.start();
        System.out.println("Simple server started...");

        try {

            Socket socket = new Socket("localhost", 1234);

            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            writer.println("GET /help HTTP/1.1");
            writer.println("Host: localhost");
            writer.println();

            writer.flush();

            char[] message = new char[10_000];
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            reader.read(message);
            String ms = new String(message);
            String cl = ms.substring(ms.indexOf("Content-length:"));
            Integer dcl = Integer.decode(cl.substring("Content-length:".length() + 1, cl.indexOf("\r\n")));

            System.out.println("OUT> ");
            System.out.println(new String(message).substring(0, new String(message).indexOf("\r\n\r\n")) + "\n");
            char[] message1 = new char[dcl];
            reader.read(message1);
            System.out.println(new String(message1));
            Assert.assertEquals(new String(message1).substring(0, 108), "<html><body><h1>Method \"GET\"  " +
                    "on URI \"/help\" </h1>Used context '/help' for reading command list of this REST");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}