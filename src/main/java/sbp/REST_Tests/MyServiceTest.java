package sbp.REST_Tests;

import com.sun.net.httpserver.HttpServer;
import org.junit.Assert;
import org.junit.Test;
import sbp.JDBC.Client_JDBC;
import sbp.REST.InitialService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import static org.junit.Assert.*;

public class MyServiceTest {

    @Test
    public void handle() {

        InitialService.main(new String[]{});

        /**
         *  Checking main CRUD operations (POST, PUT, GET, DELETE)
         *
        */

        List<String> listQuery = new ArrayList<>();
        listQuery.add("POST /person/create HTTP/1.1");
        listQuery.add("POST /person HTTP/1.1");
        listQuery.add("PUT /person HTTP/1.1");
        listQuery.add("GET /person HTTP/1.1");
        listQuery.add("DELETE /person/clrAll HTTP/1.1");
        listQuery.add("DELETE /person/drop HTTP/1.1");

        for(String query: listQuery) {

            Socket socket = null;
            try {

                socket = new Socket("localhost", 1234);

                PrintWriter writer = new PrintWriter(socket.getOutputStream());
                System.out.println(query);
                String payload = null;
                if (query.equals("POST /person HTTP/1.1")) {

                    payload = "{\"name\":\"Alex\", \"city\":\"Novgorod\", \"age\":33}";
                    writer.println(query);
                    writer.println("Host: localhost");
                    writer.println("Content-Length: " + payload.length());
                } else if (query.equals("PUT /person HTTP/1.1")) {

                    payload = "name = 'Jerry' # id<20";
                    writer.println(query);
                    writer.println("Host: localhost");
                    writer.println("Content-Length: " + payload.length());
                } else if (query.equals("GET /person HTTP/1.1")) {

                    payload = "age>30";
                    writer.println(query);
                    writer.println("Host: localhost");
                    writer.println("Content-Length: " + payload.length());
                } else {

                    writer.println(query);
                    writer.println("Host: localhost");
                }
                writer.println();
                if (payload != null && !payload.equals("")) {

                    writer.println(payload);
                }

                writer.flush();

                char[] message = new char[10_000];
                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                reader.read(message);

                Assert.assertEquals(new String(message).substring(0, new String(message).indexOf("\r\n")), "HTTP/1.1 200 OK");

                System.out.println("OUT> ");
                System.out.println(new String(message));

                socket.close();

            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException ex){
                    ex.printStackTrace();
                }
            }
        }
    }
}