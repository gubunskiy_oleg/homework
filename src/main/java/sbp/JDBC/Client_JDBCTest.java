package sbp.JDBC;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicReference;

public class Client_JDBCTest {

    String myURL = "jdbc:sqlite:src/main/java/sbp/jdbc/chinook.db";

    /**
     *  created table 'Person' and drop
     */
    @Test
    public void createPersonTable() {

        Client_JDBC.createPersonTable(myURL);
        Client_JDBC.dropPersonTable(myURL);
    }

    /**
     *  created table 'Person' and drop
     */
    @Test
    public void dropPersonTable() {

        Client_JDBC.createPersonTable(myURL);
        Client_JDBC.dropPersonTable(myURL);
    }

    /**
     *  created table 'Person', added two records of 'Person', checked count of record datatable
     *  drop of table from database
     */
    @Test
    public void setPerson() {

        Client_JDBC.createPersonTable(myURL);
        Client_JDBC.setPerson(myURL, new Person("Vladimir", "Omsk", 31));
        Client_JDBC.setPerson(myURL, new Person("Sergey", "Novosibirsk", 26));
        Assert.assertEquals(Client_JDBC.extractAllPersonTable(myURL).size(), 2);
        Client_JDBC.dropPersonTable(myURL);
    }

    /**
     *  created table 'Person', added two records of 'Person', deleted record by condition
     *  checked count of record datatable, drop of table from database
     */
    @Test
    public void deletePersonData() {

        Client_JDBC.createPersonTable(myURL);
        Client_JDBC.setPerson(myURL, new Person("Vladimir", "Omsk", 31));
        Client_JDBC.setPerson(myURL, new Person("Sergey", "Novosibirsk", 26));
        Client_JDBC.deletePersonData(myURL, "city = 'Novosibirsk'");
        Assert.assertEquals(Client_JDBC.extractAllPersonTable(myURL).size(), 1);
        Client_JDBC.dropPersonTable(myURL);
    }

    /**
     *  created table 'Person', added two records of 'Person', deleted all records from table
     *  checked count of record datatable, drop of table from database
     */
    @Test
    public void deleteAllPersonData() {

        Client_JDBC.createPersonTable(myURL);
        Client_JDBC.setPerson(myURL, new Person("Vladimir", "Omsk", 31));
        Client_JDBC.setPerson(myURL, new Person("Sergey", "Novosibirsk", 26));
        Client_JDBC.deleteAllPersonData(myURL);
        Assert.assertEquals(Client_JDBC.extractAllPersonTable(myURL).size(), 0);
        Client_JDBC.dropPersonTable(myURL);
    }

    /**
     *  created table 'Person', added two records of 'Person', deleted all records from table
     *  checked count of record datatable, drop of table from database
     */
    @Test
    public void extractAllPersonTable() {

        Client_JDBC.createPersonTable(myURL);
        Client_JDBC.setPerson(myURL, new Person("Viktor", "st.Piter burg", 29));
        Client_JDBC.setPerson(myURL, new Person("Andrey", "Moscow", 36));
        Client_JDBC.setPerson(myURL, new Person("Vladimir", "Omsk", 31));
        Client_JDBC.setPerson(myURL, new Person("Sergey", "Novosibirsk", 26));
        Assert.assertEquals(Client_JDBC.extractAllPersonTable(myURL).size(), 4);
        AtomicReference<Integer> j = new AtomicReference<>(0);
               Client_JDBC.extractAllPersonTable(myURL).stream().map(person -> person.getAge()).forEach((p) -> j.updateAndGet(v -> v + p));
        Assert.assertEquals(j.toString(), "122");
        Client_JDBC.dropPersonTable(myURL);
    }

    /**
     *  created table 'Person', added two records of 'Person', extracted of records from database by condition
     *  checked count of record datatable, drop of table from database
     */
    @Test
    public void extractPersonTable() {

        Client_JDBC.createPersonTable(myURL);
        Client_JDBC.setPerson(myURL, new Person("Viktor", "st.Piter burg", 29));
        Client_JDBC.setPerson(myURL, new Person("Andrey", "Moscow", 36));
        Client_JDBC.setPerson(myURL, new Person("Vladimir", "Omsk", 31));
        Client_JDBC.setPerson(myURL, new Person("Sergey", "Novosibirsk", 26));
        Assert.assertEquals(Client_JDBC.extractPersonTable(myURL, "age >= '29'").size(), 3);
        Client_JDBC.dropPersonTable(myURL);
    }
}