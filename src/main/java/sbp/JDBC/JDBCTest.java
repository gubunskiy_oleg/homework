package sbp.JDBC;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class JDBCTest {

    String myURL = "jdbc:sqlite:src/main/java/sbp/jdbc/chinook.db";

    /**
     *  create any table and drop
     */
    @Test
    public void createTable() {

        List<String> list = new ArrayList<>();
        list.add("name varchar(32)");
        list.add("surname varchar(52)");
        list.add("weight integer");

        Assert.assertEquals(JDBC.createTable(myURL, "anyTable", list), 0);
        Assert.assertEquals(JDBC.dropTable(myURL, "anyTable"), 0);
    }

    /**
     *  create any table and drop
     */
    @Test
    public void dropTable() {

        List<String> list = new ArrayList<>();
        list.add("name varchar(32)");
        list.add("surname varchar(52)");
        list.add("weight integer");

        Assert.assertEquals(JDBC.createTable(myURL, "anyTable", list), 0);
        Assert.assertEquals(JDBC.dropTable(myURL, "anyTable"), 0);
    }

    /**
     *  create any table, add record and drop data table
     */
    @Test
    public void addRecord() {

        List<String> list = new ArrayList<>();
        list.add("name varchar(32)");
        list.add("surname varchar(52)");
        list.add("weight integer");

        Assert.assertEquals(JDBC.createTable(myURL, "anyTable", list), 0);

        List<String> listAdd = new ArrayList<>();
        listAdd.add("name");
        listAdd.add("surname");
        listAdd.add("weight");
        List<String> val = new ArrayList<>();
        val.add("Alex");
        val.add("Miheev");
        val.add("32");
        Assert.assertEquals(JDBC.addRecord(myURL, "anyTable", val, listAdd), 1);
        Assert.assertEquals(JDBC.dropTable(myURL, "anyTable"), 0);
    }

    /**
     *  create any table, add record, clear record and drop data table
     */
    @Test
    public void clearDataTable() {

        List<String> list = new ArrayList<>();
        list.add("name varchar(32)");
        list.add("surname varchar(52)");
        list.add("weight integer");

        Assert.assertEquals(JDBC.createTable(myURL, "anyTable", list), 0);

        List<String> listAdd = new ArrayList<>();
        listAdd.add("name");
        listAdd.add("surname");
        listAdd.add("weight");
        List<String> val = new ArrayList<>();
        val.add("Alex");
        val.add("Miheev");
        val.add("32");
        Assert.assertEquals(JDBC.addRecord(myURL, "anyTable", val, listAdd), 1);
        Assert.assertEquals(JDBC.clearDataTable(myURL, "anyTable", "name = 'Alex'"), 1);
        Assert.assertEquals(JDBC.dropTable(myURL, "anyTable"), 0);
    }

    /**
     *  created table 'Person', added record to database, extracted data from database,
     *  checking to source data, drop data table
     */
    @Test
    public void extractTable() {

        List<String> list = new ArrayList<>();
        list.add("name varchar(32)");
        list.add("city varchar(32)");
        list.add("age integer");

        Assert.assertEquals(JDBC.createTable(myURL, "anyTable", list), 0);

        List<String> listAdd = new ArrayList<>();
        listAdd.add("name");
        listAdd.add("city");
        listAdd.add("age");
        List<String> val = new ArrayList<>();
        val.add("Alex");
        val.add("Moscow");
        val.add("32");
        Assert.assertEquals(JDBC.addRecord(myURL, "anyTable", val, listAdd), 1);
        List<Person> result = JDBC.extractTable(myURL, "anyTable", listAdd, "name = 'Alex'");
        for (Person p: result){
            Assert.assertEquals(p.getName(), val.get(0));
            Assert.assertEquals(p.getCity(), val.get(1));
            Assert.assertEquals(p.getAge().toString(), val.get(2));
        }
        Assert.assertEquals(JDBC.dropTable(myURL, "anyTable"), 0);
    }

    /**
     *  created table 'Person', added record to database, updated of record in database
     *  extracted data from database, checking to source data, drop data table
     */
    @Test
    public void updateDataTable() {

        List<String> list = new ArrayList<>();
        list.add("name varchar(32)");
        list.add("city varchar(32)");
        list.add("age integer");

        Assert.assertEquals(JDBC.createTable(myURL, "anyTable", list), 0);

        List<String> listAdd = new ArrayList<>();
        listAdd.add("name");
        listAdd.add("city");
        listAdd.add("age");
        List<String> val = new ArrayList<>();
        val.add("Alex");
        val.add("Moscow");
        val.add("32");
        Assert.assertEquals(JDBC.addRecord(myURL, "anyTable", val, listAdd), 1);
        Assert.assertEquals(JDBC.updateDataTable(myURL, "anyTable", "age = 28", "name = 'Alex' and city = 'Moscow'"), 1);
        List<Person> result = JDBC.extractTable(myURL, "anyTable", listAdd, "name = 'Alex'");
        for (Person p: result){
            Assert.assertEquals(p.getName(), val.get(0));
            Assert.assertEquals(p.getCity(), val.get(1));
            Assert.assertEquals(p.getAge().toString(), "28");
        }

        Assert.assertEquals(JDBC.dropTable(myURL, "anyTable"), 0);
    }
}