package sbp.JDBC;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class JDBC {

    /**
     *      Создать таблицу для хранения данных об Person (из ДЗ 7,8)
     *      и реализовать основные CRUD операции для этой таблицы с использованием JDBC
     */

    /**
     *  connection to database with execute update request
     * @param myURL it's JDBC URL for source date base
     * @param sql formatted request
     * @return count of updated rows
     */
    private static int updateTable(String myURL, String sql){

        int result = -1;
        try (Connection connection = DriverManager.getConnection(myURL);
             PreparedStatement statement = connection.prepareStatement(sql);){

            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     *  formation request for create new table in database
     * @param myURL it's JDBC URL for source date base
     * @param nameTable name of table in database
     * @param list of fields of the created table
     * @return count of updated rows
     */
    public static int createTable(String myURL, String nameTable, List<String> list){

        String sql1 = "CREATE TABLE " + nameTable + " (";
        String sql2 = list.stream()
                        .map(s -> s + ",")
                        .collect(Collectors.joining());
        String sql = sql1 + sql2.substring(0, sql2.length() - 1) + ");";
        return JDBC.updateTable(myURL, sql);
    }

    /**
     *  formation request for drop table in database
     * @param myURL it's JDBC URL for source date base
     * @param nameTable name of table in database
     * @return count of updated rows
     */
    public static int dropTable(String myURL, String nameTable){

        String sql = "DROP TABLE " + nameTable;
        return JDBC.updateTable(myURL, sql);
    }

    /**
     *  formation request for add new row to table in database
     * @param myURL it's JDBC URL for source date base
     * @param nameTable name of table in database
     * @param val list of values of the added row in table
     * @param list of fields of the added row in table
     * @return count of updated rows
     */
    public static int addRecord(String myURL, String nameTable, List<String> val, List<String> list ) {

        String sql1 = "INSERT INTO " + nameTable + " (";
        String sql2 = list.stream()
                .map(s -> s + ",")
                .collect(Collectors.joining());
        String sql3 = sql1 + sql2.substring(0, sql2.length() - 1) + ") VALUES (";
        String sql4 = val.stream()
                .map(s -> "'" + s + "',")
                .collect(Collectors.joining());
        String sql = sql3 + sql4.substring(0, sql4.length() - 1) + ");";
        return JDBC.updateTable(myURL, sql);
    }

    /**
     * formation request for clearing data from a table by condition
     * @param myURL it's JDBC URL for source date base
     * @param nameTable name of table in database
     * @param condition selection condition from the table
     * @return count of updated rows
     */
    public static int clearDataTable(String myURL, String nameTable, String condition){

        String sql = "DELETE FROM " + nameTable + (condition!=null?" WHERE " + condition:"");
        return JDBC.updateTable(myURL, sql);
    }

    /**
     *  formation request for get data from a table by condition
     * @param myURL it's JDBC URL for source date base
     * @param nameTable name of table in database
     * @param list of fields of the selected table
     * @param condition selection condition from the table
     * @return result of request selection to database
     */
    public static List<Person> extractTable(String myURL, String nameTable, List<String> list, String condition) {
        String sql1 = "SELECT ";
        String sql2 = list.stream()
                .map(s -> s + ",")
                .collect(Collectors.joining());
        String sql = sql1 + sql2.substring(0, sql2.length() - 1) + " FROM " + nameTable
                + (condition!=null?" WHERE " + condition:"");
        return JDBC.executeTable(myURL, sql);
    }

    /**
     * connecting to a database in order to execute a select query
     * @param myURL it's JDBC URL for source date base
     * @param sql formatted request
     * @return result of request selection to database
     */
    private static List<Person> executeTable(String myURL, String sql) {

        ResultSet result = null;
        List<Person> people = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(myURL);
             PreparedStatement statement = connection.prepareStatement(sql);){

            result = statement.executeQuery();
            while (result.next()){

                String name = result.getString("name");
                String city = result.getString("city");
                Integer age = result.getInt("age");
                people.add(new Person(name, city, age));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return people;
    }

    /**
     * formation request for updating row(rows) in table of database
     * @param myURL it's JDBC URL for source date base
     * @param nameTable name of table in database
     * @param toSet to set up value of the field in table
     * @param condition selection condition from the table
     * @return count of updated rows
     */
    public static int updateDataTable(String myURL, String nameTable, String toSet, String condition) {

        String sql = "UPDATE " + nameTable + " SET " + toSet + (condition!=null?" WHERE " + condition:"");
        return JDBC.updateTable(myURL, sql);
    }
}
