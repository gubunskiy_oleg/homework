package sbp.JDBC;

import java.util.ArrayList;
import java.util.List;

public class Client_JDBC {

    /**
     * adding fields for creation table, call function to create
     * @param myURL it's JDBC URL for source date base
     */
    public static void createPersonTable(String myURL){

        List<String> list = new ArrayList<>();
        list.add("name Varchar(32)");
        list.add("city Varchar(32)");
        list.add("age Integer");
        if(JDBC.createTable(myURL, "_person", list) == 0)
            System.out.println("Table _person created successful");
    }

    /**
     * call of function to drop table of database
     * @param myURL it's JDBC URL for source date base
     */
    public static void dropPersonTable(String myURL){

        if(JDBC.dropTable(myURL, "_person") == 0)
            System.out.println("Table _person drop successful");
    }

    /**
     * adding fields for update of table,
     * call of function to adding row
     * @param myURL it's JDBC URL for source date base
     * @param onePerson
     */
    public static void setPerson(String myURL ,Person onePerson){

        List<String> list = new ArrayList<>();
        list.add("name");
        list.add("city");
        list.add("age");
        List<String> val = new ArrayList<>();
        val.add(onePerson.getName());
        val.add(onePerson.getCity());
        val.add(onePerson.getAge().toString());
        if(JDBC.addRecord(myURL, "_person", val, list) == 1)
            System.out.println("Record added successful");
    }

    /**
     * call of function to clear data of table with condition
     * @param myURL it's JDBC URL for source date base
     * @param condition selection condition from the table
     */
    public static void deletePersonData(String myURL, String condition){

        if(JDBC.clearDataTable(myURL, "_person", condition) > 0)
            System.out.println("Table _person clear successful");
    }

    /**
     * call of function to clear all data of table
     * @param myURL it's JDBC URL for source date base
     */
    public static void deleteAllPersonData(String myURL){

        String condition = null;
        if(JDBC.clearDataTable(myURL, "_person", condition) > 0)
            System.out.println("Table _person clear successful");
    }

    /**
     * adding fields for extraction data table,
     * call function to get list of person
     * @param myURL it's JDBC URL for source date base
     * @return list of person, result execution of request
     */
    public static List<Person> extractAllPersonTable(String myURL){

        String condition = null;
        List<String> list = new ArrayList<>();
        list.add("name");
        list.add("city");
        list.add("age");
        List<Person> people = JDBC.extractTable(myURL, "_person", list, condition);
        if(people != null)
            System.out.println("Table _person extract successful");
        return people;
    }

    /**
     * adding list of fields for extraction data from table,
     * call function to get list of person
     * @param myURL it's JDBC URL for source date base
     * @param condition selection condition from the table
     * @return list of person, result execution of request
     */
    public static List<Person> extractPersonTable(String myURL, String condition){

        List<String> list = new ArrayList<>();
        list.add("name");
        list.add("city");
        list.add("age");
        List<Person> people = JDBC.extractTable(myURL, "_person", list, condition);
        if(people != null)
            System.out.println("Table _person extract successful");
        return people;
    }

    /**
     * call of function to update record of table with condition
     * @param myURL it's JDBC URL for source date base
     * @param toSet to set up value of the field in table
     * @param condition selection condition from the table
     */
    private static void updateRecord(String myURL, String toSet, String condition) {

        if(JDBC.updateDataTable(myURL, "_person", toSet, condition) > 0)
            System.out.println("Table _person update successful");
    }
}
