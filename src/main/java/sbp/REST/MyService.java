package sbp.REST;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import sbp.JDBC.Person;
import org.json.JSONObject;
import sbp.JDBC.Client_JDBC;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class MyService implements HttpHandler {

    private final Client_JDBC client_jdbc;

    public MyService(Client_JDBC client_jdbc) {
        this.client_jdbc = client_jdbc;
    }

    /**
     * Method 'handle' the incoming request and generate an appropriate response.
     * for each selected query exist answered with the appropriate method
     * if GET/person with condition --> getMethod(<condition>)
     * if GET/person --> getMethod all record without condition
     * if PUT/person with condition --> putMethod(<condition>) or all record without condition
     * if POST/person --> postMethod(person) add new record to table
     * if POST/person/create --> createTableMethod()
     * if DELETE/person/drop --> dropTableMethod()
     * if DELETE/person --> clearAllMethod()
     * if DELETE/person with condition --> clearPersonMethod(<condition>)
     *
     * On each new starting created new thread
     *
     * @param exchange the exchange containing the request from the
     *                 client and used to send the response
     */
    @Override
    public void handle(HttpExchange exchange) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                OutputStream outputStream = null;
                try {

                    outputStream = exchange.getResponseBody();

                    StringBuilder htmlBuilder = new StringBuilder();

                    if (exchange.getRequestMethod().equalsIgnoreCase("GET")) {

                        ifGet(exchange, htmlBuilder);
                    } else if (exchange.getRequestMethod().equalsIgnoreCase("POST")
                            && exchange.getRequestURI().toString().equalsIgnoreCase("/person/create")) {

                        ifPostAndCreate(htmlBuilder);
                    } else if (exchange.getRequestMethod().equalsIgnoreCase("POST")) {

                        ifPost(exchange, htmlBuilder);
                    } else if (exchange.getRequestMethod().equalsIgnoreCase("PUT")) {

                        ifPut(exchange, htmlBuilder);
                    } else if (exchange.getRequestMethod().equalsIgnoreCase("DELETE")) {

                        if (exchange.getRequestURI().toString().equalsIgnoreCase("/person/drop")) {

                            ifDelAndDrop(htmlBuilder);
                        } else if (exchange.getRequestURI().toString().equalsIgnoreCase("/person/clear")) {

                            ifDelAndClear(exchange, htmlBuilder);
                        } else if (exchange.getRequestURI().toString().equalsIgnoreCase("/person/clrAll")) {

                            ifDelAndClrAll(htmlBuilder);
                        }
                    } else {

                        ifCommandMismatch(htmlBuilder);
                    }

                    String htmlStr = htmlBuilder.toString();
                    exchange.sendResponseHeaders(200, htmlStr.length());

                    outputStream.write(htmlStr.getBytes(StandardCharsets.UTF_8));
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {

                    e.printStackTrace();
                } finally {

                    try {
                        outputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void ifCommandMismatch(StringBuilder htmlBuilder) {

        htmlBuilder.append("<html>")
                .append("<body>")
                .append("<h1>")
                .append("Command type mismatch, enter /help for getting info/")
                .append("</h1>")
                .append("</body>")
                .append("</html>");
    }

    private void ifDelAndClrAll(StringBuilder htmlBuilder) {

        htmlBuilder.append("<html>")
                .append("<body>")
                .append("<h1>")
                .append("Update person in DB: " + clearAllMethod())
                .append("</h1>")
                .append("</body>")
                .append("</html>");
    }

    private void ifDelAndClear(HttpExchange exchange, StringBuilder htmlBuilder) {

        String condition =
                new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                        .lines()
                        .collect(Collectors.joining());

        htmlBuilder.append("<html>")
                .append("<body>")
                .append("<h1>")
                .append("Update person in DB: " + clearPersonMethod(condition))
                .append("</h1>")
                .append("</body>")
                .append("</html>");
    }

    private void ifDelAndDrop(StringBuilder htmlBuilder) {

        htmlBuilder.append("<html>")
                .append("<body>")
                .append("<h1>")
                .append("Delete table 'person' in DB: " + dropTableMethod())
                .append("</h1>")
                .append("</body>")
                .append("</html>");
    }

    private void ifPut(HttpExchange exchange, StringBuilder htmlBuilder) {

        String data =
                new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                        .lines()
                        .collect(Collectors.joining());

        String[] sp = data.split("#");

        htmlBuilder.append("<html>")
                .append("<body>")
                .append("<h1>")
                .append("Update person in DB: " + putMethod(sp[0], sp[1]))
                .append("</h1>")
                .append("</body>")
                .append("</html>");
    }

    private void ifPost(HttpExchange exchange, StringBuilder htmlBuilder) {

        String jsonPerson =
                new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                        .lines()
                        .collect(Collectors.joining());

        JSONObject jsonObject = new JSONObject(jsonPerson);

        String name = jsonObject.toMap().get("name").toString();
        String city = jsonObject.toMap().get("city").toString();
        int age = jsonObject.getInt("age");
        Person person = new Person(name, city, age);

        htmlBuilder.append("<html>")
                .append("<body>")
                .append("<h1>")
                .append("Create new person in DB: " + postMethod(person))
                .append("</h1>")
                .append("</body>")
                .append("</html>");
    }

    private void ifPostAndCreate(StringBuilder htmlBuilder) {

        htmlBuilder.append("<html>")
                .append("<body>")
                .append("<h1>")
                .append("Create new person table in DB: " + createTableMethod())
                .append("</h1>")
                .append("</body>")
                .append("</html>");
    }

    private void ifGet(HttpExchange exchange, StringBuilder htmlBuilder) {

        String ifCondition =
                new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                        .lines()
                        .collect(Collectors.joining());

        htmlBuilder.append("<html>")
                .append("<body>")
                .append("<h1>")
                .append("All persons from database's table Persons: " + getMethod(ifCondition))
                .append("</h1>")
                .append("</body>")
                .append("</html>");
    }

    /**
     * if DELETE/person with condition --> clearPersonMethod(<condition>)
     * Called procedure delete record/records by the condition
     *
     * @param condition string of condition to database's table
     * @return string result of execution
     */
    String clearPersonMethod(String condition) {

        if (this.client_jdbc.deletePersonData(condition)){

            return "SUCCESS";
        } else {
            return "FAIL";
        }
    }

    /**
     * if DELETE/person --> clearAllMethod()
     * Called procedure delete all records from the table 'person'
     *
     * @return string result of execution
     */
    String clearAllMethod() {
        try
        {
            if (this.client_jdbc.deleteAllPersonData()){

                return "SUCCESS";
            } else {
                return "false";
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "false";
        }
    }

    /**
     * if DELETE/person/drop --> dropTableMethod()
     * Called procedure deleting of the table 'person' from database
     *
     * @return string result of execution
     */
    String dropTableMethod() {
        try
        {
            if (this.client_jdbc.dropPersonTable()){

                return "SUCCESS";
            } else {
                return "false";
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "false";
        }
    }

    /**
     * if POST/person/create --> createTableMethod()
     * Called procedure creating of the table 'person' to database
     *
     * @return string result of execution
     */
    String createTableMethod() {
        try
        {
            if (this.client_jdbc.createPersonTable()){

                return "SUCCESS";
            } else {
                return "false";
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "false";
        }
    }

    /**
     * if PUT/person with condition --> putMethod(<condition>) or all record without condition
     * Called procedure updating to the record/records of the table 'person'
     *
     * @param toSet string of updating data for record/records
     * @param condition string of condition to database's table
     * @return string result of execution
     */
    String putMethod(String toSet, String condition) {

        if (this.client_jdbc.updateRecord(toSet, condition)){

            return "SUCCESS";
        } else {
            return "FAIL";
        }
    }

    /**
     * if GET/person with condition --> getMethod(<condition>)
     * if GET/person --> getMethod all record without condition
     * Called procedure getting all record/records matching to condition from the table 'person'
     *
     * @param condition string of condition to database's table
     * @return string result of execution (JSON table record/records)
     */
    String getMethod(String condition) {

        if (!condition.equals("")) {

            return this.client_jdbc.extractPersonTable(condition).toString();
        } else {

            return this.client_jdbc.extractAllPersonTable().toString();
        }
    }

    /**
     * if POST/person --> postMethod(person) add new record to table
     * Called procedure adding record to the table 'person'
     *
     * @param person data for new record of table (type of 'person')
     * @return string result of execution
     */
    String postMethod(Person person)
    {
        try
        {
            if (this.client_jdbc.setPerson(person)){

                return "SUCCESS";
            } else {
                return "false";
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "false";
        }
    }
}
