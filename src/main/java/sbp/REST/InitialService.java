package sbp.REST;

import com.sun.net.httpserver.HttpServer;
import sbp.JDBC.Client_JDBC;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

public class InitialService {

    private static final String HOSTNAME = "localhost";
    private static final int PORT = 1234;
    private static final String db = "jdbc:sqlite:src/main/java/sbp/jdbc/chinook.db";
    /**
     * Initialize of server. Cycle adoption incoming http messages.
     */
    public static void main(String[] args) {

        HttpServer server = null;
        try {
            server = HttpServer.create(new InetSocketAddress(HOSTNAME, PORT), 0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        server.createContext("/help", new HelpHandler());
        server.createContext("/person", new MyService(new Client_JDBC(db)));
        server.setExecutor(Executors.newSingleThreadExecutor());

        server.start();
        System.out.println("Simple server started...");
    }
}
