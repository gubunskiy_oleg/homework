package sbp.REST;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class HelpHandler implements HttpHandler
{
    /**
     * on response a '/help' outputting help information about commands of 'HTTP Service'
     *
     * @param exchange the exchange containing the request from the client and used to send the response
     * @throws IOException signals that an I/O exception of some sort has occurred.
     */

    @Override
    public void handle(HttpExchange exchange)
    {

        OutputStream outputStream = null;
        try {

            outputStream = exchange.getResponseBody();

            StringBuilder htmlBuilder = new StringBuilder();
            htmlBuilder.append("<html>")
                    .append("<body>")
                    .append("<h1>")
                    .append("Method \"" + exchange.getRequestMethod() + "\" ")
                    .append(" on URI \"" + exchange.getRequestURI().getPath() + "\" ")
                    .append("</h1>")
                    .append("Used context '/help' for reading command list of this REST \n")
                    .append("Used GET in context '/person' for execution request 'select * person' to SQL BD \n")
                    .append("Used GET in context '/person' with condition in body \"age>35\" for execution request 'select * person with conditions' to SQL BD \n")
                    .append("Used PUT in context '/person' with setting value and condition in body \"city = 'Moscow' # age>35 \" (# is splitter) \n\t\t\tfor execution request 'update person set where <condition>' to SQL BD \n")
                    .append("Used POST in context '/person/create' for create new person table of database\n")
                    .append("Used POST in context '/person' with person in body \"{\"name\":\"Dukat\", \"city\":\"Leurok\", \"age\":31}\" for add new record BD \n")
                    .append("Used DELETE in context '/person/drop' for drop person table of database\n")
                    .append("Used DELETE in context '/person/clear' for delete record/records with condition in body \"age>35\" from 'person' table of database\n")
                    .append("Used DELETE in context '/person/clrAll' for delete all records from 'person' table of database\n")
                    .append("</body>")
                    .append("</html>");
            String htmlStr = htmlBuilder.toString();
            exchange.sendResponseHeaders(200, htmlStr.length());

            outputStream.write(htmlStr.getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
            outputStream.close();
        } catch (IOException ex) {

            ex.printStackTrace();
        } finally {

            try {
                outputStream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
