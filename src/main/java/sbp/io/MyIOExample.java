package sbp.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

     /**
     *  Написать реализации всех методов в классе MyIOExample
     *  с применением NIO2 (подробности в javadocs над каждым методом)
     *
     *  Написать unit тесты;
     *  Написать java docs;
      *
     */
public class MyIOExample
{
    /**
     * Создать объект класса {@link java.nio.file}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     *      - размер
     *      - время последнего изменения
     * Необходимо использовать класс {@link java.nio.file}
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName)
    {
        if (fileName.equals("") || fileName.equals(null)) return false;
        Path path = Paths.get(fileName);

        if (Files.exists(path)){

            System.out.println("path.toAbsolutePath()> " + path.toAbsolutePath());
            System.out.println("path.toAbsolutePath().getParent()> "
                    + path.toAbsolutePath().getParent());
        }

        if (Files.isRegularFile(path)){

            try {
                System.out.println("File capacity: " + Files.size(path) + " bytes");
                long time = Files.getLastModifiedTime(path).toMillis();
                System.out.printf("Last file modified: %tT %tD \n", time, time);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (Files.exists(path) && Files.isRegularFile(path)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать NIO классы
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName)
    {
        if (!checkingSourceFileName(sourceFileName)
                || !checkingDestinationFileName(destinationFileName)) {

            return false;
        }

        try {
            Files.copy(Paths.get(sourceFileName), Paths.get(destinationFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return workWithFile(destinationFileName);
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать NIO классы {@link java.io.BufferedReader} и {@link java.io.BufferedWriter}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName)
    {

        if (!checkingSourceFileName(sourceFileName)
                || !checkingDestinationFileName(destinationFileName)) {

            return false;
        }
        Path sourcePath = Paths.get(sourceFileName);
        Path destinationPath = Paths.get(destinationFileName);

        try (BufferedReader reader = Files.newBufferedReader(sourcePath, StandardCharsets.UTF_8);
             BufferedWriter writer = Files.newBufferedWriter(destinationPath, Charset.forName("UTF-8")))
        {
            String currentLine = null;
            while ((currentLine = reader.readLine()) != null) {

                writer.write(currentLine + "\n");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return workWithFile(destinationFileName);
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать NIO stream()
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName)
    {

        if (!checkingSourceFileName(sourceFileName)
                || !checkingDestinationFileName(destinationFileName)) {

            return false;
        }
        Path sourcePath = Paths.get(sourceFileName);
        Path destinationPath = Paths.get(destinationFileName);

        try {

            Files.writeString(destinationPath ,Files.lines(sourcePath)
                .map(s -> s + "\n")
                .collect(Collectors.joining()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return workWithFile(destinationFileName);
    }

    private boolean checkingSourceFileName(String sourceFileName){

        if (sourceFileName.equals("") || sourceFileName.equals(null)) return false;

        Path sourcePath = Paths.get(sourceFileName);

        if (!Files.exists(sourcePath) || !Files.isRegularFile(sourcePath)) return false;
        return true;
    }

    private boolean checkingDestinationFileName(String destinationFileName){

        if (destinationFileName.equals("") || destinationFileName.equals(null)) return false;

        Path destinationPath = Paths.get(destinationFileName);

        if (Files.exists(destinationPath) && Files.isRegularFile(destinationPath))
        {
            System.out.println("File with name " + destinationFileName + " already exist");
            return false;
        } else if (Files.isDirectory(destinationPath)) {

            System.out.println(destinationFileName + " is directory!");
            return false;
        }
        return true;
    }
}
