package sbp.io;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class MyIOExampleTest {

    /**
     *  Checking file exist, name is empty, non-existent file, name directory
     */
    @Test
    public void workWithFile() {
        MyIOExample ioExample = new MyIOExample();
        Assert.assertTrue(ioExample.workWithFile("src\\main\\java\\sbp\\JDBC\\JDBC.java"));
        Assert.assertFalse(ioExample.workWithFile(""));
        Assert.assertFalse(ioExample.workWithFile(new String()));
        Assert.assertFalse(ioExample.workWithFile("newString.txt"));
        Assert.assertFalse(ioExample.workWithFile("src"));
    }

    /**
     *  Copy check instead of an existing file,
     *  checking for copying in the path of an existing directory,
     *  checking coping file with create new file.
     */
    @Test
    public void copyFile() {
        MyIOExample ioExample = new MyIOExample();
        Assert.assertFalse(ioExample.copyFile("..\\homework\\src\\main\\"
                + "java\\sbp\\JDBC\\JDBC.java", "newFile.java"));
        Assert.assertFalse(ioExample.copyFile("..\\homework\\src\\main\\"
                + "java\\sbp\\JDBC\\JDBC.java", "src"));
        Assert.assertTrue(ioExample.copyFile("newFile.java", "test.txt"));
        try {
            Files.delete(Paths.get("test.txt"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     *  Copy check instead of an existing file,
     *  checking for copying in the path of an existing directory,
     *  checking coping file with create new file.
     */
    @Test
    public void copyBufferedFile() {
        MyIOExample ioExample = new MyIOExample();
        Assert.assertFalse(ioExample.copyBufferedFile("..\\homework\\src\\main\\"
                + "java\\sbp\\JDBC\\JDBC.java", "newFile.java"));
        Assert.assertFalse(ioExample.copyBufferedFile("..\\homework\\src\\main\\"
                + "java\\sbp\\JDBC\\JDBC.java", "src"));
        Assert.assertTrue(ioExample.copyBufferedFile("newFile.java", "test.txt"));
        try {
            Files.delete(Paths.get("test.txt"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     *  Copy check instead of an existing file,
     *  checking for copying in the path of an existing directory,
     *  checking coping file with create new file.
     */
    @Test
    public void copyFileWithReaderAndWriter() {
        MyIOExample ioExample = new MyIOExample();
        Assert.assertFalse(ioExample.copyFileWithReaderAndWriter("..\\homework\\src\\main\\"
                + "java\\sbp\\JDBC\\JDBC.java", "newFile.java"));
        Assert.assertFalse(ioExample.copyFileWithReaderAndWriter("..\\homework\\src\\main\\"
                + "java\\sbp\\JDBC\\JDBC.java", "src"));
        Assert.assertTrue(ioExample.copyFileWithReaderAndWriter("newFile.java", "test.txt"));
        try {
            Files.delete(Paths.get("test.txt"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}